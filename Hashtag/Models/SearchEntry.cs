namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class SearchEntry
    {
        public int TotalCount { get; set; }
        public int FriendsCount { get; set; }
        public List<string> FriendsUrl { get; set; }


        public SearchEntry()
        {
        }

        public SearchEntry(WroclawEventModel item, int totalCount, int friendsCount, List<string> friendsUrl)
        {
            TotalCount = totalCount;
            FriendsCount = friendsCount;
            this.Id = item.id;
            this.Title = item.offer.title;
            this.Location = item.location;
            this.StartDate = DateTime.Parse(item.startDate);
            Image = new Image(item.offer.mainImage);
            Categories = item.offer.categories.ToList();
            FriendsUrl = friendsUrl;

        }

        public int Id { get; set; }

        public string Title { get; set; }

        public List<Category> Categories { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Location Location { get; set; }

        public bool IsAttendant { get; set; }

        public int FriendsAttendant { get; set; }

        public bool IsInterested { get; set; }

        public int FriendsInterested { get; set; }

        public Image Image { get; set; }
        public double Distance { get; set; }
    }

    public class SearchEntryList
    {
        public SearchEntryList()
        {
            EventsList = new List<SearchEntry>();
        }

        public SearchEntryList(
            WroclawEventsModel wroclawEvents,
            Dictionary<int, List<string>> popularityForEvents,
            List<string> friends)
        {
            EventsList = new List<SearchEntry>(wroclawEvents.items.Length);
            foreach (var wroclawEventModel in wroclawEvents.items)
            {
                var total = 0;
                var friendsCount = 0;
                var friendsImageUrls = new List<string>();
                if (popularityForEvents.ContainsKey(wroclawEventModel.id))
                {
                    total = popularityForEvents[wroclawEventModel.id].Count;
                    friendsCount = popularityForEvents[wroclawEventModel.id].Count(f => friends.Contains(f));
                    friendsImageUrls = popularityForEvents[wroclawEventModel.id].Where(f => friends.Contains(f)).Select(u => "https://graph.facebook.com/" + u + "/picture").ToList();
                }

                var searchEntry = new SearchEntry(wroclawEventModel, total, friendsCount, friendsImageUrls);
                EventsList.Add(searchEntry);
            }
        }

        public List<SearchEntry> EventsList { get; set; }
    }
}