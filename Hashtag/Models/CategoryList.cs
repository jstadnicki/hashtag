using System.Collections.Generic;
using System.Linq;
using Models;

namespace Hashtag.Webapi.Controllers
{
    public class CategoryList
    {
        public CategoryList(WroclawCategories categories)
        {
            this.CategoriesList = categories.items.ToList();
        }

        public List<Category> CategoriesList { get; set; }
    }
}