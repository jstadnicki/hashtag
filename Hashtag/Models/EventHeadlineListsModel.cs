using System.Collections.Generic;
using System.Linq;

namespace Models
{
    public class EventHeadlineListsModel
    {
        public EventHeadlineListsModel(WroclawEventsModel wroclawEvents)
        {
            this.EventsList = wroclawEvents.items.Select(item => new EventHeadline(item)).ToList();
        }

        public List<EventHeadline> EventsList { get; set; }
    }
}