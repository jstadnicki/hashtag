namespace Models
{
    public class Venue
    {
        public bool disabledAccessAvailable { get; set; }
        public bool carParkAvailable { get; set; }
        public bool foodAndDrinkAvailable { get; set; }
    }
}