namespace Models
{
    public class Address
    {
        public string country { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string zipCode { get; set; }
        public bool defined { get; set; }
    }
}