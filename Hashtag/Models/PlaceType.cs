namespace Models
{
    public class PlaceType
    {
        public int Id { get; set; }
        public string Modified { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string LongName { get; set; }
        public string Alias { get; set; }
        public string Language { get; set; }
    }
}