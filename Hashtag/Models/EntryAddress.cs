namespace Models
{
    public class EntryAddress
    {
        public string Country { get; set; }

        public string City { get; set; }

        public string Zip { get; set; }

        public string Street { get; set; }

    }
}