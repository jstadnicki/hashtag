namespace Models
{
    public class Offer
    {
        public int id { get; set; }
        public string modified { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string alias { get; set; }
        public string longDescription { get; set; }
        public string externalLink { get; set; }
        public string pageLink { get; set; }
        public Type type { get; set; }
        public Category[] categories { get; set; }
        public Mainimage mainImage { get; set; }
        public Mainimage[] Images { get; set; }
        public Show show { get; set; }
        public int priority { get; set; }
        public string source { get; set; }
        public string language { get; set; }
        public string offerType { get; set; }
        public string lastPublished { get; set; }
        public string parentTitle { get; set; }
        public string shortTitle { get; set; }
    }
}