namespace Models
{
    public class EventEntry : SearchEntry
    {
        public EntryAddress Address { get; set; }


        public string LongDescription { get; set; }

        public string PageUrl { get; set; }

        public EventEntry()
        {

        }

        public EventEntry(WroclawEventModel item) : base(item,-1,-1, new System.Collections.Generic.List<string>())
        {
            LongDescription = item.offer.longDescription;
            PageUrl = item.offer.pageLink;
            if (item.offer.Images.Length > 0)
            {
                Image = new Image(item.offer.Images[0]);
            }
        }
    }
}