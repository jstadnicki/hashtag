namespace Models
{
    public class Mainimage
    {
        public string standard { get; set; }
        public string large { get; set; }
        public string thumbnail { get; set; }
        public string tile { get; set; }
        public string banner { get; set; }
        public string description { get; set; }
        public string role { get; set; }
    }
}