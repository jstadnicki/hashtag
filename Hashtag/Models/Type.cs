namespace Models
{
    public class Type
    {
        public int id { get; set; }
        public string modified { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string longName { get; set; }
        public string alias { get; set; }
        public string language { get; set; }
    }
}