using System.Linq;

namespace Models
{
    public class EventHeadline
    {
        public EventHeadline(WroclawEventModel item)
        {
            this.Id = item.id;
            this.Title = item.offer.title;
            this.EventDate = item.startDate;
            this.EventThumbnail = item.offer.mainImage.thumbnail;
            this.CategoryId = item.offer.categories.First().id;
        }

        public int CategoryId { get; set; }

        public string EventThumbnail { get; set; }

        public string EventDate { get; set; }

        public string Title { get; set; }

        public int Id { get; set; }
    }
}