namespace Models
{
    public class Image
    {
        public Image()
        {
        }

        public Image(Mainimage mainImage)
        {
            if (mainImage == null)
            {
                Standard = string.Empty;
                Large = string.Empty;
                Thumbnail = string.Empty;
                Tile = string.Empty;
                Banner = string.Empty;
                Description = string.Empty;
                return;
            }
            Standard = mainImage.standard;
            Large = mainImage.large;
            Thumbnail = mainImage.thumbnail;
            Tile = mainImage.tile;
            Banner = mainImage.banner;
            Description = mainImage.description;
        }

        public string Standard { get; set; }
        public string Large { get; set; }
        public string Thumbnail { get; set; }
        public string Tile { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }
    }
}