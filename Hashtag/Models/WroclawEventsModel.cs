namespace Models
{
    public class WroclawEventsModel
    {
        public int listSize { get; set; }
        public int pageSize { get; set; }
        public WroclawEventModel[] items { get; set; }
        public string next { get; set; }
    }
}