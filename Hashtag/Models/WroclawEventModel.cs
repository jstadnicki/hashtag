namespace Models
{
    public class WroclawEventModel
    {
        public int id { get; set; }
        public string modified { get; set; }
        public string url { get; set; }
        public string eventDuration { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public Location location { get; set; }
        public Address address { get; set; }
        public Offer offer { get; set; }
        public string placeName { get; set; }
        public Place place { get; set; }
        public bool premiere { get; set; }
        public string ticketing { get; set; }
        public bool urbancardPremium { get; set; }
        public string comment { get; set; }
        public int priceMin { get; set; }
        public int priceMax { get; set; }

        public Offer Offer { get; set; }
    }
}