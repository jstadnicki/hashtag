﻿namespace Models
{

    public class Place
    {
        public int Id { get; set; }
        public string Modified { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string PageLink { get; set; }
        public PlaceType Type { get; set; }

        public int Priority { get; set; }
        public string Source { get; set; }
        public string Language { get; set; }
        public Location Location { get; set; }
        public EntryAddress Address { get; set; }
        public string LastPublished { get; set; }
    }
}