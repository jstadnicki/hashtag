namespace Models
{
    public class Show
    {
        public string director { get; set; }
        public string design { get; set; }
        public string actors { get; set; }
        public string premiereDate { get; set; }
        public string[] actorsList { get; set; }
        public string genre { get; set; }
        public string costumeDesign { get; set; }
    }
}