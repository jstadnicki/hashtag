export class MainController {
  public creationDate: number;
  public EventApi: any;
  public $scope: any;
  public $state: any;
  
  /* @ngInject */
  constructor ($timeout, EventApi, $scope, $state) {
    this.creationDate = 1448048450993;
    this.EventApi = EventApi;
    this.$scope = $scope;
    this.activate($timeout);
    this.$state = $state;


    this.$scope.gotoEvent =  (id) => {
        this.$state.go('eventdetail', { 'id': id });
    }
  }

  /** @ngInject */
  activate($timeout) {
    var self = this;

    $timeout(() => {
      self.EventApi.GetEvents().success(function(data, status) {
       self.$scope.events = data;
     })
    }, 4000);
  }
  
  gotoEvent(id) {
        this.$state.go('event.details', { 'wid': id });
  }

}
