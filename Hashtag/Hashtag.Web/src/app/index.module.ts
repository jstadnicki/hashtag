/// <reference path="../../.tmp/typings/tsd.d.ts" />

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { EventDetailController } from './eventdetail/eventdetail.controller';
import { acmeNavbar } from '../app/components/navbar/navbar.directive';

declare var malarkey: any;
declare var moment: moment.MomentStatic;

module hashtag {
  'use strict';

  angular.module('hashtag', ['ngAnimate', 'ngCookies',
   'ngTouch', 'ngSanitize', 'ngMessages', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr'])
    // .constant('moment', moment)
    .config(config)
    .config(routerConfig)
    .constant('Api',  {
    //   url: '//hashtag.nawigacja.com/api'
      url: 'http://localhost:1604/api'
    })
    .run(runBlock)
    .service('EventApi', function ($http, Api) {
        this.GetEvents = function () {
            return $http.get(Api.url + "/Event/get");
        }
        this.GetEvent = function (id) {
            return $http.get(Api.url + "/Event/" + id);
        }
    })
    .controller('MainController', MainController)
    .controller('EventDetailController', EventDetailController)
}
