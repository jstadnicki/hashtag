﻿using System.Web.Mvc;
using Hashtag.Webapi.Services;
using Hashtag.Webapi.Services.Implementation;
using System.Threading.Tasks;
using Facebook;
using Newtonsoft.Json.Linq;

namespace Hashtag.Webapi.Controllers
{
    public class EventController : Controller
    {
        private readonly IEventService eventService;
        private readonly ITrackerService trackerService;

        public EventController() : this(new EventService(), new TrackerService())
        {

        }

        public EventController(IEventService eventService, ITrackerService trackerService)
        {
            this.eventService = eventService;
            this.trackerService = trackerService;
        }

        public ActionResult Index()
        {
            var events = eventService.GetEventsHeadlines();
            return View(events);
        }

        public ActionResult Search(EventSearchQuery search)
        {
            var events = eventService.QueryEvents(search);
            return View("Index", events);
        }

        public ActionResult Details(int id)
        {
            var eventItem = eventService.GetEventDetails(id);
            this.trackerService.AddEventToUserInterests(id);

            return View(eventItem);
        }
    }
}