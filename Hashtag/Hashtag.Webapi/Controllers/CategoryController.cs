using System.Web.Mvc;
using Hashtag.Webapi.Services;
using Hashtag.Webapi.Services.Implementation;

namespace Hashtag.Webapi.Controllers
{
    public sealed class CategoryController : Controller
    {

        private readonly ICategoryService categoryService;

        public CategoryController() : this(new CategoryService())
        {

        }

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        public ActionResult List()
        {
            var categories = categoryService.GetAllCategories();
            return PartialView("List", categories);
        }
    }
}