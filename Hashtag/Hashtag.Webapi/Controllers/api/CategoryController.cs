using System.Web.Http;
using Hashtag.Webapi.Services;
using Hashtag.Webapi.Services.Implementation;

namespace Hashtag.Webapi.Controllers.api
{
    [RoutePrefix("api/Category")]
    public sealed class CategoryController : ApiController
    {
        private readonly ICategoryService categoryService;

        public CategoryController() : this(new CategoryService())
        {

        }

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        [Route("get")]
        public IHttpActionResult Get()
        {
            var categories = categoryService.GetAllCategories();
            return Ok(categories);
        }
    }
}