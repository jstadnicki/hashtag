using Models;

namespace Hashtag.Webapi.Controllers.api
{
    using System.Web.Http;
    using Services;
    using Services.Implementation;

    [RoutePrefix("api/Event")]
    public sealed class EventController : ApiController
    {
        private readonly IEventService eventService;
        private readonly ITrackerService trackerService;

        public EventController() : this(new EventService(), new TrackerService())
        {

        }

        public EventController(IEventService eventService,ITrackerService trackerService)
        {
            this.eventService = eventService;
            this.trackerService = trackerService;
        }

        [Route("get")]
        public IHttpActionResult Get()
        {
            var events = eventService.GetEventsHeadlines();
            return Ok(events);
        }

        [Route("get")]
        public IHttpActionResult Get(int id)
        {
            var eventdetails = this.eventService.GetEventDetails(id);
            this.trackerService.AddEventToUserInterests(id);
       
            return Ok(eventdetails);
        }

        [Route("get/category/{id}")]
        [HttpGet]
        public IHttpActionResult EventsInCategory(int id)
        {
            var eventdetails = this.eventService.EventsInCategory(id);
            return Ok(eventdetails);
        }

    }
}