using System.Collections.Generic;

namespace Hashtag.Webapi.Services
{
    public interface ITrackerService
    {
        void AddEventToUserInterests(int id);
        Dictionary<int, List<string>> GetPopularityForEvents(List<int> eventsIds);
    }
}