using System.Collections.Generic;

namespace Hashtag.Webapi.Services
{
    public interface IAuthentication
    {
        string GetCurrentUserId();
        List<string> GetUserFriendsId();
    }
}