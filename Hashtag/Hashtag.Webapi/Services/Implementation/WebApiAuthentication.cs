using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hashtag.Webapi.Models;
using Microsoft.AspNet.Identity;

namespace Hashtag.Webapi.Services.Implementation
{
    public class WebApiAuthentication : IAuthentication
    {
        public string GetCurrentUserId()
        {
            var owinContext = HttpContext.Current.GetOwinContext();
            var authenticationManager = owinContext.Authentication;

            if (authenticationManager.User.Identity.IsAuthenticated)
            {
                var userid = (string)HttpContext.Current.Session["UserId"];
                return userid;
            }

            return "anonymous-user";
        }

        public List<string> GetUserFriendsId()
        {
            var owinContext = HttpContext.Current.GetOwinContext();
            var authenticationManager = owinContext.Authentication;
            if (authenticationManager.User.Identity.IsAuthenticated)
            {
                var friends = HttpContext.Current.Session["friendList"] as List<Friend>;
                if (friends != null)
                {
                    return friends.Select(f => f.Id).ToList();
                }
            }
            return new List<string>();
        }
    }
}