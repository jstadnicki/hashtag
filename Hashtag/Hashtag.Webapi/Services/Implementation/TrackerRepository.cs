using System.Collections.Generic;
using System.Linq;

namespace Hashtag.Webapi.Services.Implementation
{
    internal class TrackerRepository : ITrackerRepository
    {
        static private readonly List<KeyValuePair<string, int>> userEventsDictionary =
            new List<KeyValuePair<string, int>>();

        public void AddUserEvent(string userid, int eventId)
        {
            if (userEventsDictionary.Any(x => x.Key == userid && x.Value == eventId) == false)
            {
                userEventsDictionary.Add(new KeyValuePair<string, int>(userid, eventId));
            }
        }

        public List<KeyValuePair<string, int>> Get(List<int> eventsIds)
        {
            return userEventsDictionary.Where(item => eventsIds.Contains(item.Value)).ToList();
        }
    }
}