using Hashtag.Webapi.Controllers;
using RestSharp;

namespace Hashtag.Webapi.Services.Implementation
{
    public class WroclawApi : IWroclawApi
    {
        public string GetEvents()
        {
            var client = new RestClient("http://go.wroclaw.pl");
            var request = new RestRequest("api/v1.0/events", Method.GET);
            request.AddQueryParameter("key", "214432294688605191882999903202022676914");
            var response = client.Execute(request);
            return response.Content;
        }

        public string GetEvent(int id)
        {
            var client = new RestClient("http://go.wroclaw.pl");
            var request = new RestRequest(string.Format("api/v1.0/events/{0}", id), Method.GET);
            request.AddQueryParameter("key", "214432294688605191882999903202022676914");
            var response = client.Execute(request);
            return response.Content;
        }

        public string GetCategories()
        {
            var client = new RestClient("http://go.wroclaw.pl");
            var request = new RestRequest("api/v1.0/types/for-offers", Method.GET);
            request.AddQueryParameter("key", "214432294688605191882999903202022676914");
            var response = client.Execute(request);
            return response.Content;
        }

        public string GetEventsInCategory(int id)
        {
            var client = new RestClient("http://go.wroclaw.pl");
            var request = new RestRequest("api/v1.0/events", Method.GET);
            request.AddQueryParameter("key", "214432294688605191882999903202022676914");
            request.AddQueryParameter("category-id", id.ToString());
            var response = client.Execute(request);
            return response.Content;
        }

        public string QueryForEvents(EventSearchQuery search)
        {
            var client = new RestClient("http://go.wroclaw.pl");
            var request = new RestRequest("api/v1.0/events", Method.GET);
            request.AddQueryParameter("key", "214432294688605191882999903202022676914");
            if (search.CID != 0)
            {
                request.AddQueryParameter("category-id", search.CID.ToString());
            }
            request.AddQueryParameter("q", search.Query);
            var response = client.Execute(request);
            return response.Content;
        }
    }
}

