
using Hashtag.Webapi.Controllers;

namespace Hashtag.Webapi.Services.Implementation
{
    public class CategoryService : ICategoryService
    {
        private IWroclawApi wroclawApi;

        public CategoryService() : this(new WroclawApi())
        {

        }

        public CategoryService(IWroclawApi wroclawApi)
        {
            this.wroclawApi = wroclawApi;
        }

        public CategoryList GetAllCategories()
        {
            var json = this.wroclawApi.GetCategories();
            var categories = Newtonsoft.Json.JsonConvert.DeserializeObject<WroclawCategories>(json);
            var categoryList = new CategoryList(categories);
            return categoryList;
        }
    }
}