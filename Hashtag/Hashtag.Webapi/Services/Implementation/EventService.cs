using System.Collections.Generic;
using System.Linq;
using Hashtag.Webapi.Controllers;
using Microsoft.Ajax.Utilities;
using Models;

namespace Hashtag.Webapi.Services.Implementation
{
    public class EventService : IEventService
    {
        private IWroclawApi wroclawApi;
        private ITrackerService tracker;
        private IAuthentication authentication;

        public EventService() : this(new WroclawApi(), new TrackerService(), new WebApiAuthentication())
        {

        }

        public EventService(IWroclawApi wroclawApi, ITrackerService tracker, IAuthentication authentication)
        {
            this.wroclawApi = wroclawApi;
            this.tracker = tracker;
            this.authentication = authentication;
        }

        public SearchEntryList GetEventsHeadlines()
        {
            var jsonRaw = this.wroclawApi.GetEvents();
            var wroclawEvents = Newtonsoft.Json.JsonConvert.DeserializeObject<WroclawEventsModel>(jsonRaw);
            wroclawEvents.items = wroclawEvents.items.DistinctBy(d => d.id).ToArray();

            var eventsIds = wroclawEvents.items.Select(item => item.id).ToList();
            var popularityForEvents = this.tracker.GetPopularityForEvents(eventsIds);

            List<string> friends = this.authentication.GetUserFriendsId();

            var eventHeadlineListsModel = new SearchEntryList(wroclawEvents, popularityForEvents, friends);
            return eventHeadlineListsModel;
        }

        public EventEntry GetEventDetails(int id)
        {
            var jsonRaw = this.wroclawApi.GetEvent(id);
            var wroclawEvent = Newtonsoft.Json.JsonConvert.DeserializeObject<WroclawEventModel>(jsonRaw);
            var eventHeadlineListsModel = new EventEntry(wroclawEvent);
            return eventHeadlineListsModel;
        }

        public SearchEntryList EventsInCategory(int id)
        {
            var jsonRaw = this.wroclawApi.GetEventsInCategory(id); //todo filtering by category not working on api yet
            var wroclawEvents = Newtonsoft.Json.JsonConvert.DeserializeObject<WroclawEventsModel>(jsonRaw);
            wroclawEvents.items =
                 wroclawEvents.items.DistinctBy(d => d.id)
                .Where(d => d.offer.type.id == id)
                 .ToArray();

            var eventsIds = wroclawEvents.items.Select(item => item.id).ToList();
            var popularityForEvents = this.tracker.GetPopularityForEvents(eventsIds);
            List<string> friends = this.authentication.GetUserFriendsId();

            var eventHeadlineListsModel = new SearchEntryList(wroclawEvents, popularityForEvents, friends);
            return eventHeadlineListsModel;
        }

        public SearchEntryList QueryEvents(EventSearchQuery search)
        {
            var jsonRaw = this.wroclawApi.QueryForEvents(search);
            var wroclawEvents = Newtonsoft.Json.JsonConvert.DeserializeObject<WroclawEventsModel>(jsonRaw);
            if (search.CID != 0)
            {
                wroclawEvents.items = wroclawEvents.items.Where(item => item.offer.type.id == search.CID).ToArray();
            }

            var eventsIds = wroclawEvents.items.Select(item => item.id).ToList();
            var popularityForEvents = this.tracker.GetPopularityForEvents(eventsIds);
            List<string> friends = this.authentication.GetUserFriendsId();

            var eventHeadlineListsModel = new SearchEntryList(wroclawEvents, popularityForEvents, friends);
            return eventHeadlineListsModel;
        }
    }
}