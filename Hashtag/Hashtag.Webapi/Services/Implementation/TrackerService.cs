using System.Collections.Generic;
using System.Linq;

namespace Hashtag.Webapi.Services.Implementation
{
    public class TrackerService : ITrackerService
    {
        private IAuthentication authentication;
        private ITrackerRepository trackerRepository;

        public TrackerService():this(new TrackerRepository(),new WebApiAuthentication() )
        {
            
        }

        public TrackerService(
            ITrackerRepository trackerRepository, 
            IAuthentication authentication)
        {
            this.trackerRepository = trackerRepository;
            this.authentication = authentication;
        }

        public void AddEventToUserInterests(int eventId)
        {
            string userid = this.authentication.GetCurrentUserId();
            this.trackerRepository.AddUserEvent(userid, eventId);
        }

        public Dictionary<int, List<string>> GetPopularityForEvents(List<int> eventsIds)
        {
            var dictionary = trackerRepository.Get(eventsIds);
            var dictionary1 = dictionary.GroupBy(item => item.Value).ToDictionary(x => x.Key, x => x.Select(g => g.Key).ToList());
            return dictionary1;
        }
    }
}