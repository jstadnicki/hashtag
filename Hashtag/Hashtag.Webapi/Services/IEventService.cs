using Models;

namespace Hashtag.Webapi.Services
{
    using Controllers;

    public interface IEventService
    {
        SearchEntryList GetEventsHeadlines();
        EventEntry GetEventDetails(int id);
        SearchEntryList EventsInCategory(int id);
        SearchEntryList QueryEvents(EventSearchQuery search);
    }
}