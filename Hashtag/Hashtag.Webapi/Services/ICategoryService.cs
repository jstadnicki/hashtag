using System.Collections.Generic;
using System.Linq;
using Models;

namespace Hashtag.Webapi.Services
{
    public interface ICategoryService
    {
        CategoryList GetAllCategories();
    }

    public class CategoryList
    {
        public CategoryList()
        {
        }

        public CategoryList(WroclawCategories categories)
        {
            this.CategoriesList = categories.items.ToList();
        }

        public List<Category> CategoriesList { get; set; }
    }
}