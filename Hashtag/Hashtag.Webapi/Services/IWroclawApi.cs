using Hashtag.Webapi.Controllers;

namespace Hashtag.Webapi.Services
{
    public interface IWroclawApi
    {
        string GetEvents();
        string GetEvent(int id);
        string GetCategories();
        string GetEventsInCategory(int id);
        string QueryForEvents(EventSearchQuery search);
    }
}