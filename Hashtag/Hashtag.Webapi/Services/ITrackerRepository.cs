using System.Collections.Generic;

namespace Hashtag.Webapi.Services
{
    public interface ITrackerRepository
    {
        void AddUserEvent(string userid, int eventId);
        List<KeyValuePair<string, int>> Get(List<int> eventsIds);
    }

}