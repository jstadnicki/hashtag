﻿namespace Hashtag.Webapi.Models
{
    public class Friend
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl
        {
            get
            {
                return "https://graph.facebook.com/" + this.Id + "/picture";
            }
        }
    }
}