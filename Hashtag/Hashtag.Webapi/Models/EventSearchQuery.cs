namespace Hashtag.Webapi.Controllers
{
    public class EventSearchQuery
    {
        public string Category { get; set; }
        public int CID { get; set; }
        public string Query { get; set; }
    }
}