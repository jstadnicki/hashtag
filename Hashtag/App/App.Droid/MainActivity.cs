﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Locations;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using GalaSoft.MvvmLight.Ioc;
using Hashtag.App.Services;
using Xamarin.Geolocation;

namespace App.Droid
{
    [Activity(Label = "App",  MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);
            SimpleIoc.Default.Register<ILocationService>(() => new LocationService(this));

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new Hashtag.App.App());
        }
    }

    public class LocationService : ILocationService
    {
        private readonly Context _mainActivity;
        private LocationManager _locationManager;
        private string _locationProvider;
        private Position _location;

        public LocationService(Context mainActivity)
        {
            _mainActivity = mainActivity;
            return;

            var locator = new Geolocator(mainActivity) { DesiredAccuracy = 50 };
            //            new Geolocator (this) { ... }; on Android
            locator.GetPositionAsync(timeout: 100000).ContinueWith(t =>
            {
                Console.WriteLine("Position Status: {0}", t.Result.Timestamp);
                Console.WriteLine("Position Latitude: {0}", t.Result.Latitude);
                Console.WriteLine("Position Longitude: {0}", t.Result.Longitude);
            }, TaskScheduler.FromCurrentSynchronizationContext());


            _location = locator.GetPositionAsync(timeout: 10000).Result;

        }

        public double GetDistanceInKm(double endLat, double endLong)
        {

            //            new Geolocator (this) { ... }; on Android



            double rlat1 = Math.PI * _location.Latitude / 180;
            double rlat2 = Math.PI * endLat / 180;
            double theta = _location.Longitude - endLong;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;


            return dist * 1.609344;

        }
    }
}

