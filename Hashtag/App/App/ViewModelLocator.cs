using GalaSoft.MvvmLight.Ioc;
using Hashtag.App.ViewModels;
using Hashtag.App.Views;
using Microsoft.Practices.ServiceLocation;

namespace Hashtag.App
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<HomePageViewModel>();
            SimpleIoc.Default.Register<EventDetailsViewModel>();
            SimpleIoc.Default.Register<CategoryDetailsViewModel>();
            

        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public HomePageViewModel HomePageViewModel
        {
            get { return ServiceLocator.Current.GetInstance<HomePageViewModel>(); }
        }

        public EventDetailsViewModel EventDetailsPage
        {
            get { return ServiceLocator.Current.GetInstance<EventDetailsViewModel>(); }
        }

        public CategoryDetailsViewModel CategoryDetailsPage
        {
            get { return ServiceLocator.Current.GetInstance<CategoryDetailsViewModel>(); }
        }
    }
}