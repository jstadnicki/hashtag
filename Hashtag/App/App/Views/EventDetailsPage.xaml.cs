﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Hashtag.App.Views
{
    public partial class EventDetailsPage : ContentPage
    {
        public EventDetailsPage()
        {
            InitializeComponent();
            BindingContext= App.Locator.EventDetailsPage;
        }
    }
}
