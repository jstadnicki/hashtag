﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hashtag.App.ViewModels;
using Xamarin.Forms;

namespace Hashtag.App.Views
{
    public partial class CategoryDetailsPage : ContentPage
    {
        public CategoryDetailsPage()
        {
            InitializeComponent();
            BindingContext = App.Locator.CategoryDetailsPage;

        }

        private async void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            await (BindingContext as CategoryDetailsViewModel).ShowDetails();
        }
    }
}
