﻿using Hashtag.App.ViewModels;
using Xamarin.Forms;

namespace Hashtag.App.Views
{
    public partial class HomePage : CarouselPage
    {
        public HomePage()
        {
            InitializeComponent();
            BindingContext = App.Locator.HomePageViewModel;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            var viewModel = BindingContext as HomePageViewModel;
            if (viewModel.Events!=null)
                return;
            
            await viewModel.LoadData();
        }

        private async void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            await (BindingContext as HomePageViewModel).ShowDetails();
        }

        private async void CategoryListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            await(BindingContext as HomePageViewModel).ShowEventsByCategory();
        }
    }
}
