﻿using System.Threading.Tasks;
using Hashtag.App.ViewModels;
using Hashtag.App.Views;
using Models;
using Xamarin.Forms;

namespace Hashtag.App.Services
{
    public class NavigationService : INavigationService
    {
        private INavigation _navigation;

       

        public void Initialize(Page page)
        {
            _navigation = page.Navigation;
        }

        public async Task NavigateToEventDetails(int eventId)
        {
            var page=new EventDetailsPage();
            await (page.BindingContext as EventDetailsViewModel).SetEventId(eventId);
            await _navigation.PushAsync(page);
        }

        public async Task NavigateToEventsByCategory(Category category)
        {
            var page = new CategoryDetailsPage();
            await(page.BindingContext as CategoryDetailsViewModel).SetCategory(category);
            await _navigation.PushAsync(page);
        }
    }
}