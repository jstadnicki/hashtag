﻿namespace Hashtag.App.Services
{
    public interface ILocationService
    {
        double GetDistanceInKm(double endLat, double endLong);
    }
}