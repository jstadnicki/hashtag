﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hashtag.App.Models;

namespace Hashtag.App.Services
{
    public interface IOAuthService
    {
        Task<User> LoginAsync();
    }
}
