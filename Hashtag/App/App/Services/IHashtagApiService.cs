﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Models;
using ModernHttpClient;
using Newtonsoft.Json;

namespace Hashtag.App.Services
{
    public interface IHashtagApiService
    {
        Task<SearchEntryList> GetEvents();
        Task<EventEntry> GetEvent(int eventId);

        Task<List<Category>> GetCategories();
        Task<List<EventEntry>> GetEventsByCategory(int categoryId);
    }

    class HashtagApiService : IHashtagApiService
    {
        private const string eventsUrl = "http://hashtag.nawigacja.com/api/Event/get";
        private const string eventUrlFormat = "http://hashtag.nawigacja.com/api/Event/get?id={0}";
        private const string categoriesUrl = "http://hashtag.nawigacja.com/api/category/get";

        private const string eventsByCategoryUrlFormat = "http://hashtag.nawigacja.com/api/Event/get/category/{0}";
        private readonly HttpClient _httpClient;

        public HashtagApiService()
        {
            _httpClient = new HttpClient(new NativeMessageHandler());
        }

        public async Task<SearchEntryList> GetEvents()
        {
            var eventsJson = await _httpClient.GetStringAsync(eventsUrl);
            var events = JsonConvert.DeserializeObject<SearchEntryList>(eventsJson);
            return events;
        }

        public async Task<EventEntry> GetEvent(int eventId)
        {
            var eventUrl = string.Format(eventUrlFormat, eventId);
            var eventJson = await _httpClient.GetStringAsync(eventUrl);
            var eventEntry = JsonConvert.DeserializeObject<EventEntry>(eventJson);
            return eventEntry;
        }

        public async Task<List<Category>> GetCategories()
        {
         
            var eventJson = await _httpClient.GetStringAsync(categoriesUrl);
            var eventEntry = JsonConvert.DeserializeObject<CategoryList>(eventJson);
            return eventEntry.CategoriesList;
        }

        public async Task<List<EventEntry>> GetEventsByCategory(int categoryId)
        {
            var eventUrl = string.Format(eventsByCategoryUrlFormat, categoryId);
            var eventJson = await _httpClient.GetStringAsync(eventUrl);
            var eventEntry = JsonConvert.DeserializeObject<EventList>(eventJson);
            return eventEntry.EventsList;
        }
    }

    public class CategoryList
    {
        public List<Category> CategoriesList { get; set; }
    }

    public class EventList
    {
        public List<EventEntry> EventsList { get; set; }
    }
}
