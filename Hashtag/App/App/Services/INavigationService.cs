﻿using System.Threading.Tasks;
using Models;
using Xamarin.Forms;

namespace Hashtag.App.Services
{
    public interface INavigationService
    {
        void Initialize(Page page);

        Task NavigateToEventDetails(int eventId);
        Task NavigateToEventsByCategory(Category category);
    }
}