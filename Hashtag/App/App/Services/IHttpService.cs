﻿using System.Threading.Tasks;

namespace Hashtag.App.Services
{
    public interface IHttpService
    {
        Task<T> GetAsync<T>(string url);
    }
}