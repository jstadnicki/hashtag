﻿namespace Hashtag.App.Models
{
    public class User
    {

        public string AccessToken { get; set; }

        public string IdToken { get; set; }

        public string RefreshToken { get; set; }

        public dynamic Profile { get; set; }
    }
}