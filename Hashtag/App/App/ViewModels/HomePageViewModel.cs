﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Hashtag.App.Services;
using Models;
using System.Linq.Expressions;
using Xamarin.Forms;

namespace Hashtag.App.ViewModels
{
    public class HomePageViewModel : LoaderBaseViewModel
    {
        private readonly IHashtagApiService _hashtagApiService;
        private readonly INavigationService _navigationService;
        private readonly ILocationService _locationService;


        private List<SearchEntry> _events;
        private SearchEntry _selectedEvent;
        private List<Category> _categories;

        public HomePageViewModel(IHashtagApiService hashtagApiService, INavigationService navigationService, ILocationService locationService)
        {
            _hashtagApiService = hashtagApiService;
            _navigationService = navigationService;
            _locationService = locationService;
        }

        public async Task LoadData()
        {
            try
            {
                IsBusy = true;
                var events = await _hashtagApiService.GetEvents();
                Events = events.EventsList;
                foreach (var entry in Events)
                {
                    SetDistance(entry);
                }
                var categories = await _hashtagApiService.GetCategories();
                Categories = categories;
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void SetDistance(SearchEntry entry)
        {
          //TODO: Fix it!
           // entry.Distance = _locationService.GetDistanceInKm(entry.Location.Lattiude, entry.Location.Longitude);
        }

        public List<Category> Categories
        {
            get { return _categories; }
            set { _categories = value; RaisePropertyChanged(()=>Categories);}
        }

        public Category SelectedCategory { get; set; }

        public List<SearchEntry> Events
        {
            get { return _events; }
            set { _events = value;RaisePropertyChanged(()=>Events); }
        }

        public SearchEntry SelectedEvent
        {
            get { return _selectedEvent; }
            set { _selectedEvent = value;RaisePropertyChanged(()=>SelectedEvent); }
        }

        public async Task ShowDetails()
        {
            await _navigationService.NavigateToEventDetails(SelectedEvent.Id);
        }

        public async Task ShowEventsByCategory()
        {
            await _navigationService.NavigateToEventsByCategory(SelectedCategory);
        }
    }
}