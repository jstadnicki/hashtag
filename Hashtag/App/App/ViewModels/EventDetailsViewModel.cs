﻿using System.Threading.Tasks;
using Hashtag.App.Services;
using Models;

namespace Hashtag.App.ViewModels
{
    public class EventDetailsViewModel:LoaderBaseViewModel
    {
        private readonly IHashtagApiService _hashtagApiService;
        private EventEntry _event;

        public EventDetailsViewModel(IHashtagApiService hashtagApiService)
        {
            _hashtagApiService = hashtagApiService;
        }

        public async Task SetEventId(int eventId)
        {
            try
            {
                IsBusy = true;
                Event = await _hashtagApiService.GetEvent(eventId);

            }
            finally
            {
                IsBusy = false;
            }
        }

        public EventEntry Event
        {
            get { return _event; }
            set { _event = value;RaisePropertyChanged(()=>Event); }
        }
    }
}