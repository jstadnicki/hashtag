﻿using GalaSoft.MvvmLight;

namespace Hashtag.App.ViewModels
{
    public class LoaderBaseViewModel : ViewModelBase
    {
        public bool IsBusy
        {
            get { return _isBusy; }
            set { _isBusy = value; RaisePropertyChanged(() => IsBusy); RaisePropertyChanged(() => IsNotBusy); }
        }

        private bool _isBusy;


        public bool IsNotBusy
        {
            get { return !IsBusy; }
        }
    }
}