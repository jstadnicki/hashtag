﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hashtag.App.Services;
using Models;

namespace Hashtag.App.ViewModels
{
    public class CategoryDetailsViewModel : LoaderBaseViewModel
    {
        private readonly IHashtagApiService _hashtagApiService;
        private readonly INavigationService _navigationService;
        private EventEntry _selectedEvent;
        private Category _selectedCategory;
        public List<EventEntry> Events { get; set; }

        public EventEntry SelectedEvent
        {
            get { return _selectedEvent; }
            set { _selectedEvent = value;RaisePropertyChanged(()=>SelectedEvent); }
        }

        public CategoryDetailsViewModel(IHashtagApiService hashtagApiService, INavigationService navigationService)
        {
            _hashtagApiService = hashtagApiService;
            _navigationService = navigationService;
        }

        public async Task SetCategory(Category category)
        {
            try
            {
                IsBusy = true;
                Events = await _hashtagApiService.GetEventsByCategory(category.id);
                SelectedCategory = category;
                
            }
            finally
            {
                IsBusy = false;

            }
        }

        public Category SelectedCategory
        {
            get { return _selectedCategory; }
            set { _selectedCategory = value;RaisePropertyChanged(()=>SelectedCategory); }
        }

        public async Task ShowDetails()
        {
            _navigationService.NavigateToEventDetails(SelectedEvent.Id);
        }
    }
}