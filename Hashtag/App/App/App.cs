﻿using GalaSoft.MvvmLight.Ioc;
using Hashtag.App.Services;
using Hashtag.App.Views;
using Xamarin.Forms;

namespace Hashtag.App
{
    public class App : Application
    {
        private static ViewModelLocator _locator;

        public static ViewModelLocator Locator
        {
            get { return _locator ?? (_locator = new ViewModelLocator()); }
        }

        public static SimpleIoc IoC
        {
            get { return SimpleIoc.Default; }
        }

        public App()
        {
            var navigationService = new NavigationService();

            SimpleIoc.Default.Register<INavigationService>(() => navigationService);
            SimpleIoc.Default.Register<IHashtagApiService,HashtagApiService>();
           
            // The root page of your application
            var homePage = new HomePage();
            MainPage = new NavigationPage(homePage);

            navigationService.Initialize(MainPage);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}